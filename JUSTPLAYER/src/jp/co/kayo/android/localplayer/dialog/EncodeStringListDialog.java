package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.util.StringEncode;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.bean.EditItem;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class EncodeStringListDialog extends DialogFragment implements
        OnItemClickListener {

    private ListView mListView;
    private SharedPreferences mPref;
    private EncodeStringListAdapter mAdapter;
    boolean getall = false;
    private String mSourceString;
    private String mSourceFile;
    private ArrayList<EncodeStringItems> mEncItemList;
    private Callback mCallback;
    private String mEncodedstr;
    private String mEnc;

    public static class EncodeStringItems {
        public String str;
        public String enc;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.encoded_stringlist_view, null,
                false);

        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.encstr_select_title));
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setView(view);
        builder.setInverseBackgroundForced(new ThemeHelper().isInverseBackgroundForced(getActivity()));

        mEncItemList = null;
        Bundle args = getArguments();
        if (args != null) {
            mSourceFile = args.getString(SystemConsts.KEY_SOURCEFILE);
            try {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(getActivity(), Uri.parse(mSourceFile));
                String album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                String artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                if(StringEncode.isGarbled(album)){
                    mSourceString = album;
                } else if(StringEncode.isGarbled(artist)){
                    mSourceString = artist;
                }else{
                    mSourceString = title;
                }
            }finally{
                
            }
            mEncItemList = StringEncode.getEncodeStrings(getActivity(),
                    mSourceString);
            if (mEncItemList != null && mEncItemList.size() > 0) {
                mAdapter = new EncodeStringListAdapter(getActivity(),
                        mEncItemList);
                mListView.setAdapter(mAdapter);
            } else {
                // TODO エンコード出来ない、メッセージを出すべきか
                dismiss();
            }
        }
        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        ListView listView = (ListView) parent;
        // クリックされたアイテムを取得します
        EncodeStringItems item = (EncodeStringItems) listView
                .getItemAtPosition(position);
        mEnc = item.enc;
        mEncodedstr = item.str;

        if (mCallback != null) {
            mCallback.onEncodedStringSelect(mEncodedstr, mEnc, mSourceFile);
        }
        dismiss();
    }

    public void setCallback(Callback c) {
        this.mCallback = c;
    }

    public interface Callback {
        void onEncodedStringSelect(String s, String enc, String srcFile);
    }
}
