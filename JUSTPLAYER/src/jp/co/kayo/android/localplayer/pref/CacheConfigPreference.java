package jp.co.kayo.android.localplayer.pref;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.text.DecimalFormat;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CacheConfigPreference extends PreferenceActivity {
    private ProgressBar progressBar;
    private TextView txtStorageUsed;
    private TextView txtStorageCache;
    private TextView txtStorageFree;
    private TextView txtStorageMax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cache_conf);
        addPreferencesFromResource(R.xml.cache_pref);

        progressBar = (ProgressBar) findViewById(R.id.barStorage);
        txtStorageUsed = (TextView) findViewById(R.id.txtStorageUsed);
        txtStorageCache = (TextView) findViewById(R.id.txtStorageCache);
        txtStorageFree = (TextView) findViewById(R.id.txtStorageFree);
        txtStorageMax = (TextView) findViewById(R.id.txtStorageMax);

        setSdCardInfo();

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (nm != null) {
            nm.cancel(R.string.txt_notify_info);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setSdCardInfo() {
        SdCardAccessHelper sdHelper = new SdCardAccessHelper();
        String path = Environment.getExternalStorageDirectory()
                .getAbsolutePath();
        StatFs statFs = new StatFs(path);

        DecimalFormat df = new DecimalFormat(",##0.0");

        double maxSizeMB = ((double) statFs.getBlockSize() * (double) statFs
                .getBlockCount()) / (1048576);
        double cacheSizeMB = (sdHelper
                .calcDirSize(SdCardAccessHelper.cachedMusicDir) + sdHelper
                .calcDirSize(SdCardAccessHelper.cachedAlbumartDir)) / (1048576);
        double freeSizeMB = ((double) statFs.getBlockSize() * (double) statFs
                .getAvailableBlocks()) / (1048576);
        double usedSizeMB = maxSizeMB - freeSizeMB - cacheSizeMB;

        txtStorageMax
                .setText(String.format(
                        getString(R.string.lb_cache_storage_max),
                        df.format(maxSizeMB)));
        txtStorageCache.setText(String.format(
                getString(R.string.lb_cache_storage_cache),
                df.format(cacheSizeMB)));
        txtStorageFree.setText(String.format(
                getString(R.string.lb_cache_storage_free),
                df.format(freeSizeMB)));
        txtStorageUsed.setText(String.format(
                getString(R.string.lb_cache_storage_used),
                df.format(usedSizeMB)));
        progressBar.setMax((int) maxSizeMB);
        progressBar.setProgress((int) (usedSizeMB));
        progressBar.setSecondaryProgress((int) (usedSizeMB + cacheSizeMB));
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference) {
        String viewTitle = preference.getTitle().toString();
        final SdCardAccessHelper sdHelper = new SdCardAccessHelper();

        if (preference.getKey().equals(SystemConsts.KEY_DELETE_MUSIC)) {
            DialogInterface.OnClickListener dialogImpl = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Logger.d("CacheConfigPreference$DialogClick :" + which);
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        sdHelper.rmdir(SdCardAccessHelper.cachedMusicDir);
                        setSdCardInfo();
                    } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                        dialog.cancel();
                    }
                }
            };
            alertDialogCreator(viewTitle,
                    getString(R.string.txt_cache_delete_music_confirm),
                    dialogImpl);
        } else if (preference.getKey().equals(SystemConsts.KEY_DELETE_ALBUMART)) {
            DialogInterface.OnClickListener dialogImpl = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Logger.d("CacheConfigPreference$DialogClick :" + which);
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        sdHelper.rmdir(SdCardAccessHelper.cachedAlbumartDir);
                        setSdCardInfo();
                    } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                        dialog.cancel();
                    }
                }
            };
            alertDialogCreator(viewTitle,
                    getString(R.string.txt_cache_delete_albumart_confirm),
                    dialogImpl);
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void alertDialogCreator(String title, String mes,
            DialogInterface.OnClickListener dialogImpl) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(mes);
        builder.setPositiveButton(getString(R.string.lb_ok), dialogImpl);
        builder.setNegativeButton(getString(R.string.lb_cancel), dialogImpl);
        builder.setCancelable(true);
        AlertDialog dlg = builder.create();
        dlg.show();
    }
}
