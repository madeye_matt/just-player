package jp.co.kayo.android.localplayer.util;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONObject;

public class LyricsHelper {
    private static final String BASE_URL = "http://lyrics.wikia.com/api.php?";

    // private static final String REFERER = "http://yokmama.blogspot.com/";

    public String getSong(String artist, String title) throws Exception {
        // http://lyrics.wikia.com/api.php?artist=Slipknot&song=Before_I_Forget&fmt=json

        StringBuilder buf = new StringBuilder();
        buf.append(BASE_URL);
        buf.append("artist=").append(URLEncoder.encode(artist, "UTF-8"));
        buf.append("&song=").append(URLEncoder.encode(title, "UTF-8"));
        buf.append("&fmt=json");

        String json = getJSON(buf.toString());
        if (json != null && json.length() > 0) {
            JSONObject jsonobj = new JSONObject(json.replace("song =", ""));
            if (jsonobj != null) {
                /*
                 * System.out.println(jsonobj.get("artist"));
                 * System.out.println(jsonobj.get("song"));
                 * System.out.println(jsonobj.get("lyrics"));
                 * System.out.println(jsonobj.get("url"));
                 */
                return jsonobj.getString("url");
            }
        }
        return null;
    }

    public static String getJSON(String target) throws Exception {
        BufferedReader in = null;
        try {
            // URLクラスのインスタンスを生成
            URL url = new URL(target);

            System.out.println(url.toString());
            // 接続します
            URLConnection con = url.openConnection();
            // con.setRequestProperty("Referer", REFERER);

            // 入力ストリームを取得
            in = new BufferedReader(new InputStreamReader(con.getInputStream(),
                    "UTF-8"));

            // 一行ずつ読み込みます
            StringBuffer json = new StringBuffer();
            String line = null;
            while ((line = in.readLine()) != null) {
                // 表示します
                json.append(line);
            }
            return json.toString();
        } finally {
            // 入力ストリームを閉じます
            if (in != null)
                in.close();
        }
    }

}
