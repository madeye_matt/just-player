package jp.co.kayo.android.localplayer.util;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class NetworkHelper {
    public static boolean isWifiConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();

        if (info != null) {
            return ((info.getType() == ConnectivityManager.TYPE_WIFI) && (info
                    .isConnected()));
        }

        return false;
    }

    public static String getIPAddress(Context context) throws IOException {
        if (isWifiConnected(context)) {
            return getWifiIPAddress(context);
        } else {
            return get3GIPAddress();
        }
    }

    public static String get3GIPAddress() throws IOException {
        Enumeration<NetworkInterface> interfaces = NetworkInterface
                .getNetworkInterfaces();

        while (interfaces.hasMoreElements()) {
            NetworkInterface network = interfaces.nextElement();
            Enumeration<InetAddress> addresses = network.getInetAddresses();

            while (addresses.hasMoreElements()) {
                String address = addresses.nextElement().getHostAddress();
                Logger.d(address);
                if (!address.startsWith(":") && !"127.0.0.1".equals(address)
                        && !"0.0.0.0".equals(address)) {
                    return address;
                }
            }
        }

        return null;
    }

    public static String getWifiIPAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();
        String strIPAddess = ((ipAddress >> 0) & 0xFF) + "."
                + ((ipAddress >> 8) & 0xFF) + "." + ((ipAddress >> 16) & 0xFF)
                + "." + ((ipAddress >> 24) & 0xFF);
        return strIPAddess;
    }
}
