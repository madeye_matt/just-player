package jp.co.kayo.android.localplayer.core;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import android.graphics.Bitmap;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import jp.co.kayo.android.localplayer.util.ImageObserver;
import jp.co.kayo.android.localplayer.util.ViewCache;

public class ImageObserverImpl implements ImageObserver {
    ViewHolder mholder;
    Handler mHandler;
    ViewCache mViewCache;
    Integer mKey;
    int mPotision;

    public ImageObserverImpl(Handler handler, ViewCache viewcache,
            ViewHolder holder, Integer key, int pos) {
        this.mHandler = handler;
        this.mViewCache = viewcache;
        this.mholder = holder;
        this.mKey = key;
        this.mPotision = pos;
    }

    @Override
    public void onLoadImage(final Bitmap bmp) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mPotision == mholder.getPotision()) {
                    AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                    anim.setDuration(200);
                    mholder.getImage1().startAnimation(anim);
                    mholder.getImage1().setImageBitmap(bmp);
                    mholder.getImage1().invalidate();
                }
            }
        });
    }
}